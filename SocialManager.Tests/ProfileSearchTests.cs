﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Runtime.InteropServices;
using Moq;
using Newtonsoft.Json;
using NUnit.Framework;
using RestSharp;
using SocialManager.Services;
using SocialManager.Utilities;

namespace SocialManager.Tests
{
    [TestFixture]
    class ProfileSearchTests
    {
        private IProfileSearch _profileSearchService;
        private Mock<IAppSettings> _appSettingsMock;
        private Mock<IRestClient> _restClientMock;
        private string _parameterNameSettingKey;
        private string _parameterName;
        private object _graphApiObject;
        private IRestResponse _graphApiResponse;
        private string _fbUserName;
        

        [TestFixtureSetUp]
        public void TestFixtureSetup()
        {
            _parameterName = "ids";
            _parameterNameSettingKey = "facebook:graphApiQueryParameter";
            _fbUserName = "sabby.baath";
            _graphApiObject = new {id = 1, first_name = "Sarfraz", last_name = "Singh", link = "", name = "Sarfraz Singh"};
            _graphApiResponse = new RestResponse { StatusCode = HttpStatusCode.OK, Content = JsonConvert.SerializeObject(_graphApiObject)};
            
        }

        [SetUp]
        public void SetUp()
        {
            _appSettingsMock = new Mock<IAppSettings>();
            _restClientMock = new Mock<IRestClient>();

            _appSettingsMock.Setup(x => x.Get(_parameterNameSettingKey)).Returns(_parameterName);
            _restClientMock.Setup(
              x =>
                  x.Execute(
                      It.Is<IRestRequest>(r => r.Method.Equals(Method.GET))))
               .Returns(_graphApiResponse);

            _profileSearchService = new ProfileSearch(_appSettingsMock.Object, _restClientMock.Object);
        }

        [Test]
        public void Ctor_ShouldGet_SearchParameterNameFrom_AppSettings()
        {
            _appSettingsMock.Verify(x => x.Get(_parameterNameSettingKey));
        }

        [Test]
        public void GetProfile_ShouldCallExecute_InRestClient()
        {
            _profileSearchService.GetProfiles(_fbUserName);
            _restClientMock.Verify(x => x.Execute(It.Is<IRestRequest>(r => r.Method.Equals(Method.GET))));
        }

        [Test]
        public void GetProfile_ShouldCallExecute_WithCorrectQueryParameter()
        {
            _profileSearchService.GetProfiles(_fbUserName);
            _restClientMock.Verify(x => x.Execute(It.Is<IRestRequest>(r => r.Parameters.Find(p => p.Name.Equals("ids")).Value.Equals(_fbUserName))));
        }

        [TestCase(null)]
        [TestCase("")]
        [TestCase(" ")]
        [ExpectedException(typeof(ArgumentException))]
        public void GetProfile_ThrowsArgumentException_WhenInvalidIds(string ids)
        {
            _profileSearchService.GetProfiles(ids);
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void GetProfile_ThrowsArgumentException_WhenSearchFails()
        {
            var graphApiInvalidResponse = new RestResponse() {StatusCode = HttpStatusCode.BadRequest, Content = "Search Failed"};
            _restClientMock.Setup(
                x =>
                    x.Execute(
                        It.Is<IRestRequest>(r => r.Method.Equals(Method.GET))))
                .Returns(graphApiInvalidResponse);
            _profileSearchService.GetProfiles(_fbUserName);
        }
    }
}
