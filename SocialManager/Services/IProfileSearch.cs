﻿using Newtonsoft.Json.Linq;

namespace SocialManager.Services
{
    public interface IProfileSearch
    {
        JObject GetProfiles(string ids);
    }
}
