﻿using System;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using SocialManager.Utilities;

namespace SocialManager.Services
{
    public class ProfileSearch : IProfileSearch
    {
        private const string QueryParameterNameSettingsKey = "facebook:graphApiQueryParameter";
        private readonly string _queryParameterName;
        private readonly IRestClient _restClient;

        #region Constructor

        public ProfileSearch(IAppSettings settings, IRestClient restClient)
        {
            _queryParameterName = settings.Get(QueryParameterNameSettingsKey);
            _restClient = restClient;
        }

        #endregion

        /// <summary>
        /// Gets the profiles.
        /// </summary>
        /// <param name="ids">The ids.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentException">Response retured by GraphAPI was:  + response.StatusCode</exception>
        public JObject GetProfiles(string ids)
        {
            if (string.IsNullOrWhiteSpace(ids))
            {
                throw new ArgumentException("Ids cannot be null or empty space", ids);
            }
            var request = new RestRequest(Method.GET) { RequestFormat = DataFormat.Json };
            request.AddQueryParameter(_queryParameterName, ids);
            var response = _restClient.Execute(request);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                return JObject.Parse(response.Content);
            }
            else
            {
                throw new ArgumentException("Response retured by GraphAPI was: " + response.StatusCode);
            }
        }

    }
}
