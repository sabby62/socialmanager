﻿namespace SocialManager.Utilities
{
    public interface IAppSettings
    {
        string Get(string key);
    }
}
