﻿using System.Configuration;
using SocialManager.Utilities;

namespace SocialManager.API.Utilities
{
    public class AppSettings: IAppSettings
    {
        public string Get(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }
    }
}