﻿using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using System.Web.Http.Dispatcher;
using RestSharp;
using SocialManager.API.IOC;
using SocialManager.API.Utilities;
using SocialManager.Services;
using SocialManager.Utilities;
using StructureMap;
using StructureMap.Graph;

namespace SocialManager.API
{
    public class WebApiApplication : HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            IContainer container = ConfigureDependencies();
            GlobalConfiguration.Configuration.Services.Replace(typeof(IHttpControllerActivator), new StructureMapHttpControllerActivator(container));
        }

        #region Structure Map

        private static IContainer ConfigureDependencies()
        {
           var container = new Container();

            container.Configure(x => x.For<IProfileSearch>().Use<ProfileSearch>());
            container.Configure(x => x.For<IAppSettings>().Use<AppSettings>());
            container.Configure(x => x.For<IRestClient>().Use(new RestClient("https://graph.facebook.com/")));

            return container;
        }

       

        #endregion
    }
}
