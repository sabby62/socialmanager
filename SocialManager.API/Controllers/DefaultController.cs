﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;
using SocialManager.Services;

namespace SocialManager.API.Controllers
{
    
    public class DefaultController : ApiController
    {
        private readonly IProfileSearch _profileSearch;

        public DefaultController(IProfileSearch profileSearch)
        {
            _profileSearch = profileSearch;
        }

        [AcceptVerbs("GET")]
        [Route("~/")]
        public HttpResponseMessage GetProfileInfo(HttpRequestMessage request)
        {
            return request.CreateResponse(HttpStatusCode.OK, "Use /profile?ids={profileid}     *without curly braces*");
        }

        [AcceptVerbs("GET")]
        [Route("profile", Name = "DefaultRoute")]
        public HttpResponseMessage GetProfileInfo(HttpRequestMessage request, string ids)
        {
            try
            {
                var response = request.CreateResponse(HttpStatusCode.OK, _profileSearch.GetProfiles(ids));
                return response;
            }
            catch (ArgumentException e)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
            catch (Exception)
            {
                return request.CreateResponse(HttpStatusCode.InternalServerError);
            }

        }
    }
}