**Scenario:**
The company is building a social media based application to gather information about people.  
**Overview**
Write a web application that gathers data from any user’s public Facebook profile from the Facebook API. 
Implementation
1.	GET the information for a single user based on the Facebook username or Id. The username or Id would be specified as a parameter by the user of the web application as a part of the GET request. Return the Json formatted response containing all the information returned by Facebook.
2.	Get the information for multiple users based on the Facebook usernames or Ids.  These usernames or Ids would be specified as a parameter by the user of the web application as a part of the GET request. Return the Json formatted response containing all the information returned by Facebook.

**Constraints and Requirements**
You are free to use any development environment, tools, and resources that you see fit. Your solution should, however, fulfill the following requirements:
•	Use C# and the .NET framework (4.5  or above)
•	Create a web application, using an MVC or REST framework
•	Focus on SOLID principles, unit tests, and code organization
•	Code quality will be evaluated as if it were ready for release to production.

Note:-
This coding exercise is meant to be finished in less than 3 hours.

**IMPLEMENTATION DETAILS**

[https://bitbucket.org/sabby62/socialmanager](https://bitbucket.org/sabby62/socialmanager)

    Web application : ASP.Net Web Api 2.2
    Language : C# / .Net 4.5
    Solution : SocialManager.sln